#!/bin/bash

perl -0 -ne 'chomp; m!^([0-9]+)/([0-9]+)/(.*)$!s or next; utime $1, $2, $3;' \
    "$1"/dates.dat
mkdir -p "$1" && {
    : ${RUSTUP_HOME:=${HOME}/.rustup} ${CARGO_HOME:=${HOME}/.cargo}
    RUSTUP_OLD="$(dirname "$RUSTUP_HOME")/old_$(basename "$RUSTUP_HOME")"
    CARGO_OLD="$(dirname "$CARGO_HOME")/old_$(basename "$CARGO_HOME")"
    aptg="apt-get -y -qq"
    [ "$(type -p -t rsync)" == "file" ] || $aptg update &&
        $aptg --no-install-suggests --no-install-recommends install rsync
    rsync --archive --hard-links --update \
        "${RUSTUP_HOME}/." "$1"/.rustup &&
        mv -f "$RUSTUP_HOME" "$RUSTUP_OLD" &&
        ln -s "$(pwd)"/"$1"/.rustup "$RUSTUP_HOME" &
    rsync --archive --hard-links --update \
        "${CARGO_HOME}/." "$1"/.cargo &&
        mv -f "$CARGO_HOME" "$CARGO_OLD" &&
        ln -s "$(pwd)"/"$1"/.cargo "$CARGO_HOME"
    wait
}
for nothg in a b; do
    perl -0 -ne 'chomp; my $t = <>; chomp $t; if (-e $_ && -e $t) {'\
' my ($st, $tt) = ((lstat($_))[9], (lstat($t))[9]); if ($st > $tt) {'\
' unlink $t; link $_, $t; }; if ($st < $tt) { unlink $_; link $t, $_; };'\
' if ($st == $tt) { if ((lstat($_))[1] != (lstat($t))[1]) { unlink $t;'\
' link $_, $t; }; }; } else { -e $t || link $_, $t; -e $_ || link $t, $_;'\
' }' "$1"/hardlinks.dat
done
